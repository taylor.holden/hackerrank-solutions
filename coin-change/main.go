package main

import (
	"fmt"
	"sort"
)

func main() {
	//Enter your code here. Read input from STDIN. Print output to STDOUT
	var N, M int

	fmt.Scanf("%d %d\n", &N, &M)

	coins := make([]int, M)
	memo := make(map[int]map[int]int)

	for i := 0; i < M; i++ {
		memo[i] = make(map[int]int)
		fmt.Scanf("%d", &coins[i])
	}

	memo[M] = make(map[int]int)

	sort.Ints(coins)
	fmt.Printf("%d\n", countCombinations(N, M, &coins, &memo))
}

func countCombinations(total, numCoins int, coins *[]int, memo *map[int]map[int]int) int {
	if _, ok := (*memo)[numCoins][total]; !ok {
		if total == 0 {
			return 1
		}

		if total < 0 {
			return 0
		}

		if numCoins <= 0 && total > 0 {
			return 0
		}

		(*memo)[numCoins][total] = countCombinations(total-(*coins)[numCoins-1], numCoins, coins, memo) + countCombinations(total, numCoins-1, coins, memo)
	}

	return (*memo)[numCoins][total]
}
