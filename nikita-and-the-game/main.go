package main

import "fmt"

func main() {
	var numCases, numElements int

	fmt.Scanf("%d\n", &numCases)

	for i := 0; i < numCases; i++ {
		fmt.Scanf("%d\n", &numElements)
		array := readArrayElements(numElements)

		result := playGame(array, sum(array))
		fmt.Printf("%d\n", result)
	}
}

func readArrayElements(numElements int) *[]int {
	array := make([]int, numElements)

	for i := 0; i < numElements; i++ {
		fmt.Scanf("%d", &array[i])
	}
	fmt.Scanf("\n")

	return &array
}

func playGame(array *[]int, rightSum int) (result int) {
	if len(*array) <= 1 {
		return 0
	}

	leftSum := 0
	for index, value := range *array {
		leftSum += value
		rightSum -= value

		if leftSum == rightSum {
			leftSide := (*array)[0 : index+1]
			rightSide := (*array)[index+1 : len(*array)]
			result = 1 + max(playGame(&leftSide, rightSum), playGame(&rightSide, rightSum))
			break
		} else if leftSum > rightSum {
			result = 0
			break
		}
	}

	return
}

func sum(array *[]int) (result int) {
	for _, value := range *array {
		result += value
	}

	return
}

func max(a, b int) int {
	if a > b {
		return a
	}

	return b
}
