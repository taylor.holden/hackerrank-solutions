package main

import (
	"fmt"
	"math/big"
)

func main() {
	//Enter your code here. Read input from STDIN. Print output to STDOUT
	var (
		t1, t2 int64
		n      int
	)
	fmt.Scanf("%d %d %d\n", &t1, &t2, &n)

	memo := map[int]*big.Int{
		1: big.NewInt(t1),
		2: big.NewInt(t2),
	}

	fmt.Printf(fibb(n, &memo).String())
}

func fibb(n int, memo *map[int]*big.Int) *big.Int {
	if n <= 2 {
		return (*memo)[n]
	}

	if _, ok := (*memo)[n]; !ok {
		nMinusOne := fibb(n-1, memo)
		nResult := big.NewInt(0)
		nResult.Mul(nMinusOne, nMinusOne)
		nResult.Add(nResult, fibb(n-2, memo))
		(*memo)[n] = nResult
	}

	return (*memo)[n]
}
