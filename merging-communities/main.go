package main

import (
	"bufio"
	"fmt"
	"os"
	"strings"
)

func main() {
	var people []*Community
	var numberOfPeople, numberOfQueries int

	fmt.Scanf("%d %d\n", &numberOfPeople, &numberOfQueries)

	people = make([]*Community, numberOfPeople, numberOfPeople)
	for i := 0; i < numberOfPeople; i++ {
		people[i] = NewCommunity()
	}

	var personI, personJ int
	reader := bufio.NewReader(os.Stdin)

	for i := 0; i < numberOfQueries; i++ {
		line, _ := reader.ReadString('\n')
		//fmt.Print(line)
		if strings.Index(line, "M") > -1 {
			fmt.Sscanf(line, "M %d %d", &personI, &personJ)
			//fmt.Printf("Running: M %d %d\n", personI, personJ)
			Merge(people[personI-1], people[personJ-1])
		} else {
			fmt.Sscanf(line, "Q %d", &personI)
			// fmt.Printf("Running: Q %d\n", personI)
			fmt.Printf("%d\n", people[personI-1].Size())
		}
	}
	// fmt.Println(people)
	// fmt.Printf("%d\n", people[0].Size())
	// Merge(people[0], people[1])
	// fmt.Printf("%d\n", people[1].Size())
	// Merge(people[1], people[2])
	// fmt.Printf("%d\n", people[2].Size())
	// fmt.Printf("%d\n", people[1].Size())
}

type Community struct {
	parent *Community
	size   int
}

func NewCommunity() *Community {
	temp := Community{size: 1}
	temp.parent = &temp

	return &temp
}

func (c *Community) FindParent() *Community {
	if c.getParent() != c {
		// fmt.Printf("%p .FindParent(); getParent() = %p", c, c.getParent())
		c.setParent(c.getParent().FindParent())
	}
	return c.getParent()
}

func Merge(i, j *Community) *Community {
	iParent := i.FindParent()
	jParent := j.FindParent()

	// fmt.Printf("Merge: %s %s\n", i, j)
	// fmt.Printf("Parents: %s %s\n", iParent, jParent)

	if iParent != jParent {
		// fmt.Printf("%p %p disjoint\n", i, j)
		if iParent.getSize() >= jParent.getSize() {
			jParent.setParent(iParent)
			iParent.setSize(iParent.getSize() + jParent.getSize())
			// fmt.Println(iParent)
			return iParent
		} else {
			iParent.setParent(jParent)
			jParent.setSize(iParent.getSize() + jParent.getSize())
			// fmt.Println(jParent)
			return jParent
		}
	}

	return iParent
}

func (c *Community) Size() int {
	if c != nil {
		return c.FindParent().getSize()
	}
	return 0
}

func (c *Community) getParent() *Community {
	if c == nil {
		return nil
	}

	return (*c).parent
}

func (c *Community) setParent(p *Community) {
	if c != nil {
		(*c).parent = p
	}
}

func (c *Community) getSize() int {
	if c == nil {
		return 0
	}
	return (*c).size
}

func (c *Community) setSize(size int) {
	if c != nil {
		(*c).size = size
	}
}
