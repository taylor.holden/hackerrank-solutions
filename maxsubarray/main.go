package main

import "fmt"

func main() {
	//Enter your code here. Read input from STDIN. Print output to STDOUT
	var numTests, numValues int

	fmt.Scanf("%d\n", &numTests)

	for i := 0; i < numTests; i++ {
		fmt.Scanf("%d\n", &numValues)

		array := readIntsIntoArray(numValues)

		maxContiguous, maxNonContiguous := findMaxSubarraySums(array)
		fmt.Printf("%d %d\n", maxContiguous, maxNonContiguous)
	}
}

func readIntsIntoArray(n int) *[]int {
	array := make([]int, n)

	for i := 0; i < n; i++ {
		fmt.Scanf("%d", &array[i])
	}
	fmt.Scanf("\n")

	return &array
}

func findMaxSubarraySums(array *[]int) (contiguous, nonContiguous int) {
	var currentValue int
	length := len(*array)
	maxContiguous := (*array)[0]
	maxNonContiguous := (*array)[0]

	for i := 1; i < length; i++ {
		currentValue = (*array)[i]
		maxNonContiguous = max(max(maxNonContiguous, maxNonContiguous+currentValue), currentValue)
		(*array)[i] = max(currentValue, currentValue+(*array)[i-1])
		maxContiguous = max((*array)[i], maxContiguous)
	}

	return maxContiguous, maxNonContiguous
}

func max(a, b int) int {
	if a > b {
		return a
	}

	return b
}
