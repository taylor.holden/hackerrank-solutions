package main
import (
    "bufio"
    "fmt"
    "math/rand"
    "os"
)

func main() {
    var arraySize, numberOfQueries, nodeValue int
    fmt.Scanf("%d %d\n%d", &arraySize, &numberOfQueries, &nodeValue)
    root := NewNode(nodeValue)

    for i := 2; i <= arraySize; i++ {
        fmt.Scanf("%d", &nodeValue)
        //fmt.Printf("Inserting %d\n", nodeValue)
        root = root.Insert(nodeValue, i)
    }

    //fmt.Scanf("\n")

    //fmt.Print("Initial: ")
    //PrettyPrintList(root)
    //fmt.Println("Root: ", root.string())
    reader := bufio.NewReader(os.Stdin)

    var queryType, from, to int
    for i := 0; i < numberOfQueries; i++ {
        line, _ := reader.ReadString('\n')
        fmt.Sscanf(line, "%d %d %d", &queryType, &from, &to)

        //fmt.Printf("Query: type: %d, from %d, to: %d\n", queryType, from, to)

        if queryType == 1 {
            root = root.moveRangeToFront(from, to)
        } else {
            root = root.moveRangeToBack(from, to)
        }
        //fmt.Println("After query: ")
        //PrettyPrintList(root)
        //PrintList(root)
    }

    firstNode := root.getFirstNode()
    lastNode := root.getLastNode()
    abs := firstNode.value - lastNode.value
    if abs < 0 {
        abs = abs * -1
    }

    fmt.Printf("%d\n", abs)

    PrintNodeInOrder(root)
    //fmt.Println()
}

type TreapNode struct
{
    priority, subtreeSize, value int
    left, right *TreapNode
}

func NewNode(value int) *TreapNode {
    temp := &TreapNode{
        subtreeSize: 1,
        value: value,
        priority: rand.Int()%10000,
        left: nil,
        right: nil,
    }

    return temp
}

func (node *TreapNode) recalculateSize() {
    if node != nil {
        node.subtreeSize = node.left.getSize() + 1 + node.right.getSize()
    }
}

func (root *TreapNode) split(position, add int) (L, R *TreapNode) {
    if root == nil {
        return nil, nil
    }
    var l, r *TreapNode

    currentPosition := add + root.left.getSize()

    if position >= currentPosition {
        root.right, r = root.right.split(position, currentPosition + 1)
        root.recalculateSize()
        return root, r
    } else {
        l, root.left = root.left.split(position, add)
        root.recalculateSize()
        return l, root
    }
}

func merge(left, right *TreapNode) *TreapNode {
    if left == nil {
        return right
    } else if right == nil {
        return left
    }

    if left.getPriority() > right.getPriority() {
        left.right = merge(left.right, right)
        left.recalculateSize()
        return left
    } else {
        right.left = merge(left, right.left)
        right.recalculateSize()
        return right
    }
}

func (root *TreapNode) Insert(value, index int) *TreapNode {
    left, right := root.split(index-1, 0)
    return merge(merge(left, NewNode(value)), right)
}

func (node *TreapNode) getSize() int {
    if node == nil {
        return 0
    }

    return node.subtreeSize
}

func (node *TreapNode) getPriority() int {
    if node == nil {
        return -1
    }
    return node.priority
}

func (root *TreapNode) extractRange(i, j int) (LeftSide, Range, RightSide *TreapNode) {
    var L, ret, R *TreapNode

    L, R = root.split(i-2, 0)
    ret, R = R.split(j-i, 0)
    return L, ret, R
}

func (root *TreapNode) moveRangeToFront(i, j int) *TreapNode {
    //fmt.Printf("Moving values from %d to %d to the front.\n", i, j);
    left, myRange, right := root.extractRange(i, j)
    //fmt.Println("Left: ")
    //PrettyPrintList(left)
    //fmt.Println("Extracted: ")
    //PrettyPrintList(myRange)
    //fmt.Println("Right: ")
    //PrettyPrintList(right)
    return merge(myRange, merge(left, right))
}

func (root *TreapNode) moveRangeToBack(i, j int) *TreapNode {
    //fmt.Printf("Moving values from %d to %d to the back.\n", i, j);
    left, myRange, right := root.extractRange(i, j)
    //fmt.Println("Left: ")
    //PrettyPrintList(left)
    //fmt.Println("Extracted: ")
    //PrettyPrintList(myRange)
    //fmt.Println("Right: ")
    //PrettyPrintList(right)
    return merge(left, merge(right, myRange))
}

func PrintNodeInOrder(node *TreapNode) {
    if node != nil {
        PrintNodeInOrder(node.left)
        fmt.Print(node.string())
        PrintNodeInOrder(node.right)
    }
}

func (root *TreapNode) getFirstNode() *TreapNode {
    for root.left != nil {
        root = root.left
    }

    return root
}

func (root *TreapNode) getLastNode() *TreapNode {
    for root.right != nil {
        root = root.right
    }

    return root
}

func (node *TreapNode) string() string {
    return fmt.Sprintf("%d ", node.value)
    //return fmt.Sprintf("[Size:%d, Value:%d] ", node.getSize(), node.value)
    //return fmt.Sprintf("{Priority:%d, SubtreeSize: %d, Value: %d} ", node.priority, node.subtreeSize, node.value)
}

func PrintList(root *TreapNode) {
    PrintNodeInOrder(root)
    fmt.Println()
}

func PrettyPrintList(root *TreapNode) {
    PrettyPrintNode(root, 0)
    fmt.Print("\n\n")
}

func PrettyPrintNode(node *TreapNode, depth int) {
    if node != nil {
        PrettyPrintNode(node.left, depth+1)
        var padding string
        for i:= 0; i < 5*depth; i++ {
            padding += "-"
        }
        padding += ">"
        fmt.Println(padding, node.string())
        PrettyPrintNode(node.right, depth+1)
    }
}
